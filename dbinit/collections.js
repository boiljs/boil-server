const {
  If,
  Exists,
  Collection,
  Replace,
  CreateCollection
} = require("faunadb")
const NAMES = require("../src/db/names.json")


const createOrReplateCollection = ({ name }) => If(
  Exists(Collection(name)),
  Replace(Collection(name), { name }),
  CreateCollection({ name })
)

const users = createOrReplateCollection({ name: NAMES.COLLECTIONS.USERS })
const plates = createOrReplateCollection({ name: NAMES.COLLECTIONS.PLATES })

module.exports = [users, plates]
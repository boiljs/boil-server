require("dotenv").config()
const { Client, Create, Collection, If, IsNonEmpty, Paginate, Match, Index, Select, Do, Delete, Ref } = require("faunadb")
const NAMES = require("../src/db/names.json")

// TODO DB init seeding
const c = new Client({ secret: process.env.FAUNA_ADMIN_KEY })

/** @type {{data: {email: string, username: string}, credentials: {password: string}}[]} */
const users = [
  {
    data: {
      email: "a@example.com",
      username: "user_a",
    },
    credentials: { password: "passwordA1" }
  }
]
  .map(({ data, credentials }) => Do(
    If(
      IsNonEmpty(Paginate(Match(Index(NAMES.INDEXES.USER.UNIQUE_EMAIL), data.email))),
      Delete(Select(0, Paginate(Match(Index(NAMES.INDEXES.USER.UNIQUE_EMAIL), data.email)))),
      0
    ),
    Create(Collection(NAMES.COLLECTIONS.USERS), { data, credentials })
  ))


console.log("........Seeding DB........")
c.query(
  ...users
)
  .catch(e => {
    console.log("xxxxx Seeding Failed xxxxx")
    console.log(e)
  })
  .finally(() => process.exit())
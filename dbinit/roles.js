const {
  Client,
  Update,
  Collection,
  Role,
  Query,
  If,
  Exists,
  CreateRole,
  Lambda,
  Equals,
  CurrentIdentity,
  Var,
  Select,
  Let,
  Get,
  Or,
  Index,
} = require("faunadb")
const NAMES = require("../src/db/names.json")

const createOrUpdateRole = ({ name, membership, privileges }) => If(
  Exists(Role(name)),
  Update(Role(name), { name, membership, privileges }),
  CreateRole({ name, membership, privileges })
)

const logged_in_user = createOrUpdateRole({
  name: NAMES.ROLES.LOGGED_IN,
  membership: [{ resource: Collection(NAMES.COLLECTIONS.USERS) }],
  privileges: [
    {
      resource: Collection(NAMES.COLLECTIONS.USERS),
      actions: {
        read: Query(Lambda("userRef", Equals(CurrentIdentity(), Var("userRef")))),
        write: Query(Lambda("userRef", Equals(CurrentIdentity(), Var("userRef")))),
        delete: Query(Lambda("userRef", Equals(CurrentIdentity(), Var("userRef"))))
      }
    }
  ]
})

const guest = createOrUpdateRole({
  name: NAMES.ROLES.GUEST,
  privileges: [
    {
      resource: Collection(NAMES.COLLECTIONS.PLATES),
      actions: {
        read: Query(
          Lambda(
            "plateRef",
            Let(
              {
                plateDoc: Get(Var("plateRef")),
              },
              Select(["data", "public"], Var("plateDoc"), true)
            )
          )
        ),
      }
    }
  ]
})

/** @param {Client} c */
const content_manager = createOrUpdateRole({
  // TODO content manager role
})

module.exports = [
  logged_in_user
]
{
  Users: {
    ref: Ref(Collection(Users), "id")
    data: {
      email: string // unique
      username: string // unique
      defaultPackage: {
        name: string
        description: string
        version: string
        author: AuthorType
        main: string
        repository: RepositoryType
        license: string
        bugs: BugsType
        homepage: string
        dependencies: <string, string>
        devDependencies: <string, string>
        funding: FundingType | string | (FundingType | string)[]
        scripts: <string, string>
      }
      role: "GENERIC" | "CONTENT_MANAGER" | "ADMIN"
    }
  }
  Plates: {
    ref: Ref(Collection(Plates), "id")
    data: {
      userID: string
      name: string // unique relative to user
      public: boolean
      tags: string[]
      package: {
        name: string
        description: string
        version: string
        author: AuthorType
        main: string
        repository: RepositoryType
        license: string
        bugs: BugsType
        homepage: string
        dependencies: <string, string>
        devDependencies: <string, string>
        funding: FundingType | string | (FundingType | string)[]
        scripts: <string, string>
      }
    }
    fileStructure: FolderType
  }
}
const { createOrUpdateIndex } = require("./util")
const NAMES = require("../../src/db/names.json")
const { Collection } = require("faunadb")

const all_users = createOrUpdateIndex({
  name: NAMES.INDEXES.USER.ALL,
  source: Collection(NAMES.COLLECTIONS.USERS)
})

const user_unique_email = createOrUpdateIndex({
  name: NAMES.INDEXES.USER.UNIQUE_EMAIL,
  source: Collection(NAMES.COLLECTIONS.USERS),
  unique: true,
  terms: [{ field: ["data", "email"] }]
})

const user_unique_username = createOrUpdateIndex({
  name: NAMES.INDEXES.USER.UNIQUE_NAME,
  source: Collection(NAMES.COLLECTIONS.USERS),
  unique: true,
  terms: [{ field: ["data", "username"] }]
})

const user_role = createOrUpdateIndex({
  name: NAMES.INDEXES.USER.ROLE,
  source: Collection(NAMES.COLLECTIONS.USERS),
  terms: [{ field: ["data", "role"] }]
})

module.exports = [
  all_users,
  user_unique_email,
  user_unique_username,
  user_role
]
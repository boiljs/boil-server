const plates = require("./plates")
const user = require("./user")

module.exports = [
  ...plates,
  ...user
]
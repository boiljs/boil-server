const {
  Collection,
  Query,
  Lambda,
  Select,
  Var
} = require("faunadb")
const { createOrUpdateIndex } = require("./util")
const NAMES = require("../../src/db/names.json")

const all_plates = createOrUpdateIndex({
  name: NAMES.INDEXES.PLATE.ALL,
  source: Collection(NAMES.COLLECTIONS.PLATES)
})

const plate_name = createOrUpdateIndex({
  name: NAMES.INDEXES.PLATE.NAME,
  source: Collection(NAMES.COLLECTIONS.PLATES),
  terms: [{ field: ["data", "name"] }]
})

const plate_owner = createOrUpdateIndex({
  name: NAMES.INDEXES.PLATE.OWNER,
  source: Collection(NAMES.COLLECTIONS.PLATES),
  terms: [{ field: ["data", "userID"] }]
})

const plate_public = createOrUpdateIndex({
  name: NAMES.INDEXES.PLATE.PUBLIC,
  source: Collection(NAMES.COLLECTIONS.PLATES),
  terms: [{ field: ["data", "public"] }]
})

const plate_tags = createOrUpdateIndex({
  name: NAMES.INDEXES.PLATE.TAGS,
  source: Collection(NAMES.COLLECTIONS.PLATES),
  terms: [{ field: ["data", "tags"] }]
})

/**
 * Match(Index("plate_name_and_owner"), ["name", "username"])
 */
const plate_name_and_owner = createOrUpdateIndex({
  name: NAMES.INDEXES.PLATE.NAME_AND_OWNER,
  unique: true,
  source: {
    collection: Collection(NAMES.COLLECTIONS.PLATES),
    fields: {
      userID: Query(
        Lambda(
          "plateDoc",
          Select(["data", "userID"], Var("plateDoc"))
        )
      )
    }
  },
  terms: [
    { field: ["data", "name"] },
    { binding: "userID" }
  ]
})

module.exports = [
  all_plates,
  plate_name,
  plate_owner,
  plate_public,
  plate_name_and_owner,
  plate_tags
]
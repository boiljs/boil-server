# Boil-server

This is the API and website server for
[Boil CLI](https://boil.herokuapp.com/), a command line tool for saving npm
templates.

---

## API

TODO

---

## Roadmap

- Server
  - routing
    - Plate
      - Create
      - Update
      - View Personal
      - View Pubilc
  - DB connection
- Site
  - Wiki/Guides
  - View public Plates
    - Sort by popularity or recency
    - Searching by name
  ~~- User logins~~ [see](https://gitlab.com/boiljs/auth)
- Testing
  - complate joi schema testing
  - mongoose integration tests

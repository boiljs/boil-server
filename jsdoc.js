module.exports = {
  source: {
    include: ["dist/"],
    exclude: ["node_modules/"]
  },
  plugins: ["plugins/markdown"],
  templates: {
    cleverLinks: true,
    monospaceLinks: true
  },
  opts: {
    template: "jsdoc_template",
    recurse: true,
    destination: `./public/jsdocs/${require("./dist/Project").default.apiVersion}`
  }
}

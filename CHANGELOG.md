# Boil API & Website Change Log

## Unreleased

- api
  - added
    - Joi schemas for the following:
      - User Defaults
      - Dependencies
      - License
      - Name
      - Plate
      - Scripts
      - User
      - Version
- testing
  - added
    - version schema
    - user schema
    - scripts schema
    - name schema
    - dependencies schema
    - defaults schema

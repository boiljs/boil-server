require("dotenv").config() // Load .env
require("./logger") // Configure Logger
import "express-async-errors" // !! This must be called before express is used
import makeServer from "./server"
import logger from "winston"


// Start the server
makeServer().listen(process.env.PORT, () => {
  logger.info(`listening on port ${process.env.PORT}`)
  if (process.env.STAGE === "DEVELOPMENT") {
    logger.info(`http://127.0.0.1:${process.env.PORT}`)
  }
})
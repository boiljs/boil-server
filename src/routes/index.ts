import user from './users'
import plates from "./plates"

export default { user, plates }
import { Router } from "express"
import { requireUser } from "../middleware"
import ctlr from "../controllers"

const router = Router()

router.post("/register", ctlr.users.post.register)
router.post("/signup", ctlr.users.post.register)
router.post("/login", ctlr.users.post.login)
router.get("/", requireUser, ctlr.users.get.self)
router.get("/:user_id", requireUser, ctlr.users.get.one)
router.patch("/:user_id", requireUser, ctlr.users.patch.one)
router.delete("/:user_id", requireUser, ctlr.users.delete.one)

export default router
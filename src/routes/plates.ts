import { Router } from "express"
import { requireUser } from "../middleware"
import ctlr from "../controllers"

const router = Router()

router.post("/", requireUser, ctlr.plates.post.one)
router.get("/", ctlr.plates.get.many)
router.get("/:plate_id", ctlr.plates.get.one)
router.patch("/:plate_id", requireUser, ctlr.plates.patch.one)
router.delete("/:plate_id", requireUser, ctlr.plates.delete.one)

export default router
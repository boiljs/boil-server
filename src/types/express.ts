import { Request as ExpressRequest, Response as ExpressResponse } from "express"
import { UserType } from "../models"

export type Request = ExpressRequest & {
  token?: string
  user?: UserType
}

export type Response = ExpressResponse & {
  pack: (success: boolean, payload?: any, meta?: any) => ExpressResponse<any, Record<string, any>>
}
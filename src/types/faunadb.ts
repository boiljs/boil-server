export type Ref = {
  id: string
  collection?: Ref
  "@ref": Ref
}

export type QueryResult<DataType = any> = {
  ref?: Ref
  ts?: number
  data?: DataType
}

export type Document<DataType = any> = QueryResult<DataType>
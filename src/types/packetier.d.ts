declare module "packetier" {
  export function packetier(success: boolean, payload?: any, meta?: any): object
}
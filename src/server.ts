import express, { Application, Router } from "express"
import Project from "./Project"
import { join } from "path"
import cors from "cors"
import routes from "./routes"
import { attachPacketier, errorHandler, tryToken, tryUser } from "./middleware"

/**
 * Initialize a new Server instance.
 *
 * @returns A new Express Server instance.
 *
 * @since 0.1.0
 * @author Jonathan Augustine
 */
export default (): Application => {
  // Create new server
  const server = express()

  // Setup external middleware
  server.use(express.json()).use(cors())

  // Setup public site files
  server.use(express.static(join(__dirname, "../public")))

  // Setup public jsdocs files
  server.use("/docs", express.static(join(__dirname, "..", "public", "jsdocs", Project.apiVersion)))

  // Setup Swagger docs route
  server.use("/docs/api", async (_, res) => {
    // TODO make url version dynamic
    res.redirect(`https://app.swaggerhub.com/apis-docs/JonoAugustine/Boil/0.2.0`)
  })

  // Setup API routes
  const api = Router()
  api.use([attachPacketier, tryToken, tryUser])
  api.use("/users", routes.user)
  api.use("/plates", routes.plates)
  // TODO add api routes

  server.use(`/api/${Project.apiVersion}`, api)

  // error handling
  server.use(errorHandler)

  return server
}

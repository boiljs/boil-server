import Dao from "./dao"
import NAMES from "./names.json"

if (!process.env.FAUNA_SERVER_KEY) {
  console.log("MISSING FAUNA SERVER KEY")
  process.exit(-1)
}
if (!process.env.FAUNA_GUEST_KEY) {
  console.log("MISSING FAUNA GUEST KEY")
  process.exit(-1)
}

/**
 * Server authority DB client
 * 
 * @since 0.2.0
 * @author JonoAugustine
 */
const asServer = new Dao(process.env.FAUNA_SERVER_KEY)

/**
 * Create a DB client with the authority of the given token.
 * 
 * @since 0.2.0
 * @author JonoAugustine
 * 
 * @param token Fauna auth token
 * @returns DB Client as user
 */
const as = (token?: string) => new Dao(token || process.env.FAUNA_GUEST_KEY)

export { as, asServer, NAMES }
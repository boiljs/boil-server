import { InternalError } from "../errors"
import {
  Client,
  Collection,
  Create,
  CurrentIdentity,
  Var,
  Get,
  Ref,
  Update,
  Delete,
  Paginate,
  Map,
  Match,
  Lambda,
  Expr,
  Intersection,
  Union,
  Let,
  Do
} from "faunadb"
import { PlateType } from "../models"
import { Document } from "../types/faunadb"
import indexes from "./indexes"
import NAMES from "./names.json"

type SearchQueries = {
  userID?: string
  platename?: string
  /** ^(.+,){1,}$ */
  tags?: string[]
}

export default class PlateDao {
  private client: Client

  constructor(client: Client) {
    this.client = client
  }

  async create(plate: PlateType): Promise<PlateType> {
    const { data, ref: { id } } = await this.client.query<Document<PlateType>>(
      Create(Collection(NAMES.COLLECTIONS.PLATES), { data: plate, })
    )

    return { ...data, id }
  }

  async getById(id: string): Promise<PlateType> {
    const { data, ref: { id: pid } } = await this.client.query<Document<PlateType>>(
      Get(Ref(Collection(NAMES.COLLECTIONS.PLATES), id))
    )
    return { ...data, id: pid }
  }

  private async getByIndex<T = PlateType>(
    matches: [Expr, any?] | [Expr, any?][],
    join: "INTERSECT" | "UNION" = "INTERSECT"
  ): Promise<Array<T>> {
    const list = await this.client.query<{ data: Array<Document<T>> }>(
      Map(
        Paginate(
          Array.isArray(matches)
            ? (join === "INTERSECT" ? Intersection : Union)(
              matches.map(([index, field]) => Match(index, field ?? undefined))
            )
            : Match(matches[0], matches[1])
        ),
        Lambda("ref", Get(Var("ref")))
      )
    )

    return list.data.map(({ data, ref: { id } }) => ({ ...data, id }))
  }

  getByOwner(uid: string): Promise<Array<PlateType>> {
    return this.getByIndex([indexes.plate.owner, uid])
  }

  getByNameAndOwner(platename: string, uid: string): Promise<Array<PlateType>> {
    return this.getByIndex([indexes.plate.nameAndOwner, [platename, uid]])
  }

  getByName(platename: string): Promise<Array<PlateType>> {
    return this.getByIndex([indexes.plate.name, platename])
  }

  async search(sq: SearchQueries): Promise<Array<PlateType>> {
    const matches: [Expr, any?][] = [[indexes.plate.all]]

    if (sq.userID) matches.push([indexes.plate.owner, sq.userID])

    if (sq.platename) matches.push([indexes.plate.name, sq.platename])

    if (sq.tags) {
      sq.tags.forEach(tag => matches.push([indexes.plate.tags, tag]))
    }

    return this.getByIndex(matches, "INTERSECT")
  }

  async updateById(id: string, packet: Partial<PlateType>): Promise<PlateType> {
    const { data, ref: { id: pid } } = await this.client.query<Document<PlateType>>(
      Update(
        Ref(Collection(NAMES.COLLECTIONS.PLATES), id),
        { data: packet }
      )
    )

    return { ...data, id: pid }
  }

  async deleteById(...ids: string[]): Promise<void> {
    await this.client.query(
      Do(ids.map(id => Delete(Ref(Collection(NAMES.COLLECTIONS.PLATES), id))))
    )
  }
}

import {
  Map,
  Client,
  Collection,
  Create,
  Expr,
  Get,
  Index,
  Lambda,
  Let,
  Login,
  Match,
  Paginate,
  Ref,
  Select,
  Tokens,
  Var,
  Update,
  Delete,
  IsNonEmpty,
} from "faunadb"
import { UserType } from "../models/User"
import { Document } from "../types/faunadb"
import indexes from "./indexes"
import NAMES from "./names.json"

type AuthReturn = {
  user: UserType
  token: string
}

type FaunaAuthReturn = {
  userDoc: Document<UserType>,
  tokenDoc: { secret: string }
}

/**
 * @since 0.2.0
 * @author JonoAugustine
 */
export default class UserDao {
  private client: Client

  constructor(client: Client) {
    this.client = client
  }

  async register(user: { email: string; password: string; username?: string }): Promise<AuthReturn> {
    const { userDoc, tokenDoc } = await this.client.query<FaunaAuthReturn>(
      Let(
        {
          userDoc: Create(Collection(NAMES.COLLECTIONS.USERS), {
            credentials: { password: user.password },
            data: {
              email: user.email,
              username: user.username || null,
              role: NAMES.USER_ROLES.GENERIC,
            },
          })
        },
        {
          userDoc: Var("userDoc"),
          tokenDoc: Create(Tokens(), { instance: Select("ref", Var("userDoc")), }),
        }
      )
    )

    return {
      user: { ...userDoc.data, id: userDoc.ref.id },
      token: tokenDoc.secret
    }
  }

  /**
   * FQL#Login returns
   * ```
   * {
   *   ref: Ref(Tokens(), "268283157930836480"),
   *   ts: 1592113607250000,
   *   instance: Ref(Collection("characters"), "181388642114077184"),
   *   secret: 'fnEDuSIcV7ACAAO5IhwXkAIAMQbrsrZaHs1cUWnligxyD5kUAPE'
   * }
   * ```
   */
  private async login(
    { identity, password }: { identity: string; password: string },
    index: Expr
  ): Promise<AuthReturn> {
    const { userDoc, tokenDoc } = await this.client.query<FaunaAuthReturn>(
      Let(
        { tokenDoc: Login(Match(index, identity), { password }) },
        {
          tokenDoc: Var("tokenDoc"),
          userDoc: Get(Select("instance", Var("tokenDoc")))
        }
      )
    )

    return {
      user: { ...userDoc.data, id: userDoc.ref.id },
      token: tokenDoc.secret
    }
  }

  loginWithUsername({ username, password, }: { username: string, password: string }) {
    return this.login(
      { password, identity: username },
      Index(NAMES.INDEXES.USER.UNIQUE_NAME)
    )
  }

  loginWithEmail({ email, password }: { email: string; password: string }) {
    return this.login(
      { password, identity: email },
      Index(NAMES.INDEXES.USER.UNIQUE_EMAIL)
    )
  }

  async getByID(id: string): Promise<UserType> {
    const { data, ref: { id: uid } } = await this.client.query<Document<UserType>>(
      Get(Ref(Collection(NAMES.COLLECTIONS.USERS), id))
    )
    return { ...data, id: uid }
  }

  private async getByIndex(index: Expr, identity: any): Promise<Array<UserType>> {
    const { data } = await this.client.query<{ data: Array<Document<UserType>> }>(
      Map(
        Paginate(Match(index, identity)),
        Lambda("ref", Get(Var("ref")))
      )
    )
    return data.map(({ data, ref: { id } }) => ({ ...data, id }))
  }

  async getByUsername(username: string): Promise<UserType | undefined> {
    const [user] = await this.getByIndex(indexes.users.byName, username)
    return user
  }

  async getByEmail(email: string): Promise<UserType> {
    const [user] = await this.getByIndex(indexes.users.byEmail, email)
    return user
  }

  private existsByIndex(index: Expr, identity: any) {
    return this.client.query<boolean>(
      IsNonEmpty(Paginate(Match(index, identity)))
    )
  }

  existsByUsername(username: string) {
    return this.existsByIndex(Index(NAMES.INDEXES.USER.UNIQUE_NAME), username)
  }

  existsByEmail(email: string) {
    return this.existsByIndex(Index(NAMES.INDEXES.USER.UNIQUE_EMAIL), email)
  }

  async updateById(id: string, packet: Partial<UserType>): Promise<UserType> {
    const { data, ref: { id: uid } } = await this.client.query<Document<UserType>>(
      Update(Ref(Collection(NAMES.COLLECTIONS.USERS), id), { data: packet })
    )
    return { ...data, id: uid }
  }

  async deleteById(id: string): Promise<UserType> {
    const { data, ref: { id: uid } } = await this.client.query<Document<UserType>>(
      Delete(Ref(Collection(NAMES.COLLECTIONS.USERS), id))
    )
    return { ...data, id: uid }
  }
}

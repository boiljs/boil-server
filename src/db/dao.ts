import { Client, CurrentIdentity, Get } from "faunadb"
import { UserType } from "../models"
import { Document } from "../types/faunadb"
import PlateDao from "./plateDao"
import UserDao from "./userDao"

/**
 * Parent DAO which contains a User and Plate Dao
 * 
 * @since 0.2.0
 * @author JonoAugustine
 */
export default class Dao {
  private client: Client
  user: UserDao
  plate: PlateDao

  constructor(secret: string) {
    this.client = new Client({ secret })
    this.user = new UserDao(this.client)
    this.plate = new PlateDao(this.client)
  }

  async getIdentity(token?: string): Promise<UserType> {
    const cl = token ? new Client({ secret: token }) : this.client
    const d = await cl.query<Document<UserType>>(Get(CurrentIdentity()))
    return { ...d.data, id: d.ref.id }
  }
}
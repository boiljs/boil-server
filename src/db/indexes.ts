import { Index } from "faunadb"
import { INDEXES } from "./names.json"

export default Object.freeze({
  users: {
    byName: Index(INDEXES.USER.UNIQUE_NAME),
    byEmail: Index(INDEXES.USER.UNIQUE_EMAIL),
    role: Index(INDEXES.USER.ROLE)
  },
  plate: {
    all: Index(INDEXES.PLATE.ALL),
    tags: Index(INDEXES.PLATE.TAGS),
    name: Index(INDEXES.PLATE.NAME),
    owner: Index(INDEXES.PLATE.OWNER),
    public: Index(INDEXES.PLATE.PUBLIC),
    nameAndOwner: Index(INDEXES.PLATE.NAME_AND_OWNER)
  }
})
import { Response } from "express"
import { packetier } from "packetier"
import { as } from "../db"
import { AuthError } from "../errors"
import errorHandler from "./errorHandler"
import logger from "winston"
import { Request } from "../types/express"

export const attachPacketier = (_, res: Response, next) => {
  res["pack"] = (success: boolean, payload?: any, meta?: any) => {
    return res.json(packetier(success, payload, meta))
  }
  next()
}

/** If a token was sent in Auth or Header, set `req.token` */
export const tryToken = async (req, _, next) => {
  if (req.headers.authorization) {
    const bearer: Array<string> = req.headers.authorization.split(" ")
    if (bearer.length > 1) req.token = bearer[1]
  }
  else req.token = req.headers.token

  logger.debug(req.token ? "TOKEN FOUND" : "NO TOKEN FOUND")
  next()
}

export const requireUser = (req, _, next) => {
  if (!req.user) {
    logger.debug("MISSING REQUIRED USER INSTANCE")
    throw new AuthError("Missing or invalid token")
  }
  else next()
}


/** If a token exists on `req`, attempt to set `req.user` */
export const tryUser = async (req: Request, _, next) => {
  // Get user from token
  if (req.token) {
    try {
      req.user = await as(req.token).getIdentity()
      logger.debug("USER FOUND")
    } catch (_) {
      logger.debug("NO USER FOUND, DELETING TOKEN")
      req.token = undefined
    }
  }

  next()
}

export { errorHandler }
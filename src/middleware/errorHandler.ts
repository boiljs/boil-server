import {
  DuplicateError,
  InternalError,
  MalformedContentError,
  AuthError,
  ResourceNotFoundError
} from "../errors"
import logger from "winston"
import { Response } from "../types/express"

// TODO better error handling
const errorHandler = (error: any, _, res: Response, __: any) => {

  if (error instanceof DuplicateError) res.status(409)

  else if (error instanceof InternalError) res.status(500)

  else if (error instanceof MalformedContentError) res.status(400)

  else if (error.message === "invalid argument") {
    logger.error("Invalid Argument error", error)
    error.message = error.description
    res.status(400)
  }

  else if (error instanceof AuthError) res.status(401)

  else if (/(PermissionDenied|Unauthorized)/i.test(error.name)) {
    logger.debug("PERMISSION DENIED BY FAUNA")
    res.status(401)
  }

  else if (/document\s+was\s+not\s+found\s+or\s+provided\s+password\s+was\s+incorrect/i.test(error.description)) {
    error.message = "User not found or password incorrect"
    res.status(401)
  }

  else if (error instanceof ResourceNotFoundError) res.status(404)

  else if (error.message === "instance not found") res.status(404)

  else {
    logger.error(`UNHANDLED "${error.name || "ERROR"}"`, error)
    res.status(500)
  }

  res.pack(false, null, { error: error.message })
}

export default errorHandler
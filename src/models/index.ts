import Name from "./Name"
import User, { UserType, UserUpdatePartial } from "./User"
import Plate, { PlateType, PlateUpdatePartial } from "./Plate"

export {
  Name,
  User,
  UserType,
  UserUpdatePartial,
  PlateUpdatePartial,
  Plate,
  PlateType
}
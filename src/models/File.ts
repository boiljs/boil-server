import { object, string } from "@hapi/joi"

export default object({
  name: string().max(1024).required(),
  body: string().max(20420).optional()
})

export type FileType = {
  name: string
  body?: string
}
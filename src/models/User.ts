import { object, string } from "@hapi/joi"
import Name from "./Name"
import Package, { PackageType } from "./Package"

const schema = object({
  username: Name.optional(),
  email: string().email().required().alter({ update: (s) => s.optional() }),
  defaultPackage: Package.optional(),
})

export const UserUpdatePartial = schema.tailor("update")

export type UserType = {
  id?: string
  email: string
  username?: string
  defaultPackage?: PackageType
  role?: "GENERIC" | "CONTENT_MANAGER" | "ADMIN"
}

export default schema
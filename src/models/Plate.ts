import { array, boolean, object, string } from "@hapi/joi"
import Folder, { FolderType } from "./Folder"
import Name from "./Name"
import Package, { PackageType } from "./Package"

const schema = object({
  name: Name.required().alter({ update: s => s.optional() }),
  public: boolean().default(false),
  tags: array().items(string().min(3).max(20).pattern(/^[a-z0-9][\s+a-z0-9\-]+$/i)),
  package: Package.required().alter({ update: s => s.optional() }),
  fileStructure: Folder.optional()
})

export const PlateUpdatePartial = schema.tailor("update")

export type PlateType = {
  id?: string
  userID: string
  name: string,
  public?: boolean
  tags?: Array<string>
  package?: PackageType
  fileStructure?: FolderType
}

export default schema
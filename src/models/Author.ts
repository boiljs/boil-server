import { alternatives, object, string } from "@hapi/joi"

export default alternatives().try(
  string().max(214).allow(''),
  object({
    name: string().max(63),
    email: string().email(),
    url: string().uri()
  })
)

export type AuthorType = string | {
  name?: string
  email?: string
  url?: string
}
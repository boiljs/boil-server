import { object, string } from "@hapi/joi"

export default object({
  type: string().max(214),
  url: string().uri()
})

export type FundingType = { type?: string, url?: string }
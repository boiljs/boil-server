import { array, link, object, string } from "@hapi/joi"
import File, { FileType } from "./File"

export default object({
  name: string().max(1024).required(),
  files: array().items(File).optional(),
  folders: array().items(link("#folder")).optional()
}).id("folder")

export type FolderType = {
  name: string,
  files?: Array<FileType>,
  folders?: Array<FolderType>
}
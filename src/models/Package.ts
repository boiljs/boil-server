import { alternatives, array, object, string } from "@hapi/joi"
import Author, { AuthorType } from "./Author"
import Bugs, { BugsType } from "./Bugs"
import Dependency from "./Dependency"
import Funding, { FundingType } from "./Funding"
import Repository, { RepositoryType } from "./Repository"

export default object({
  name: string().max(214).optional().allow(''),
  description: string().optional().max(1024).allow(''),
  version: string().max(100).optional().allow(''),
  author: Author.optional(),
  main: string().max(255).optional().allow(''),
  repository: Repository.optional(),
  license: string().max(64).optional().allow(''),
  bugs: Bugs.optional(),
  homepage: string().uri().allow(''),
  dependencies: Dependency.optional(),
  devDependencies: Dependency.optional(),
  funding: alternatives().try(
    Funding,
    string().uri(),
    array().items(Funding, string().uri())
  ),
  scripts: object().optional().custom((value) => {
    for (const k in value) {
      if (!/^[0-9a-z\-\.]+$/i.test(k)) throw new Error("invalid script name")
      if (typeof value[k] !== "string" || value[k].length > 1024) throw new Error("invalid script value")
    }
    return value
  }),
})

export type PackageType = {
  name?: string
  description?: string
  version?: string
  author?: AuthorType
  main?: string
  repository?: RepositoryType
  license: string
  bugs?: BugsType
  homepage: string
  dependencies?: Record<string, string>
  devDependencies?: Record<string, string>
  funding?: FundingType | string | Array<FundingType | string>
  scripts?: Record<string, string>
}
import { object, string } from "@hapi/joi"

export default object({
  name: string().optional(),
  url: string().uri().optional()
})
export type RepositoryType = { name?: string, url?: string }
import { object, string } from "@hapi/joi"

export default object({
  url: string().uri(),
  email: string().email()
})

export type BugsType = { url?: string, email?: string }
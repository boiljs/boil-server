import { object } from "@hapi/joi"

export default object().custom((value) => {
  for (const k in value) {
    if (typeof value[k] !== "string" || value[k].length > 1024)
      throw new Error("invalid dependency value")
  }
  return value
})
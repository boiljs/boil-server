/** Current version of the server project */
const version = "0.2.0"
/** Current version of the external API */
const apiVersion = "v2"
const license = "GPL-3.0"

/**
 * Project Definition & info.
 *
 * @since 0.1.0
 * @author Jonathan Augustine
 */
export default { version, license, apiVersion }

import users from "./users"
import plates from "./plates"

export default { users, plates }
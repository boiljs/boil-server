import get from "./get"
import post from "./post"
import patch from "./patch"
import _delete from "./delete"

export default { get, post, patch, delete: _delete }
import { as, asServer } from "../../db"
import { Request, Response } from "../../types/express"
import logger from "winston"
import { AuthError } from "../../errors"

/**
 * GET /users/{user_id}
 */
const one = async (req: Request, res: Response) => {
  const { user_id } = req.params
  logger.debug("GET USER BY_ID", { user_id })

  // is self or admin
  if (req.user.id !== user_id && req.user?.role !== "ADMIN") {
    logger.debug("GET USER BY_ID UNAUTHORIZED", { user_id, client_id: req.user.id })
    throw new AuthError()
  }

  const user = await asServer.user.getByID(req.params.user_id)
  res.pack(true, user)
}

/**
 * GET /users
 */
const self = async (req: Request, res: Response) => {
  logger.debug("GET USER SELF", { id: req.user?.id })
  res.pack(typeof req.user === "object", req.user)
}

export default { one, self }
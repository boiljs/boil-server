import { as, asServer } from "../../db"
import { AuthError, MalformedContentError } from "../../errors"
import { UserUpdatePartial } from "../../models"
import { Request, Response } from "../../types/express"
import logger from "winston"

/**
 * PATCH /users/{user_id}
 */
const one = async (req: Request, res: Response) => {
  const { user_id } = req.params

  logger.debug("UPDATE USER", { clientIsTarget: user_id === req.user?.id, })

  // validate update info
  const { value, error } = UserUpdatePartial.validate(req.body, { stripUnknown: true })

  if (error) {
    logger.debug("UPDATE USER FAILED", error)
    throw new MalformedContentError(error.message)
  }

  // only allow self and admin to edit user
  if (req.user.id !== user_id && req.user.role !== "ADMIN") {
    throw new AuthError()
  }

  // Update
  const updatedUser = await asServer.user.updateById(user_id, value)

  res.pack(true, updatedUser)
}

export default { one }
import { object, string } from "@hapi/joi"
import { Name } from "../../models"
import { asServer } from "../../db"
import { DuplicateError, MalformedContentError } from "../../errors"
import { Request, Response } from "../../types/express"

const register = async (req: Request, res: Response) => {
  // validate incoming data
  const validation = object({
    email: string().email().max(1024).required(),
    username: Name.optional(),
    password: string()
      .min(8)
      .max(32)
      .pattern(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]+$/)
      .required(),
  }).validate(req.body, { stripUnknown: true })

  if (validation.error) {
    throw new MalformedContentError(
      validation.error.message.includes("password")
        ? "Password must be 8-32 characters and contain at least 1 number"
        : validation.error.message,
    )
  }

  // check for duplicates
  const { email, username } = validation.value
  if (await asServer.user.existsByEmail(email)) {
    throw new DuplicateError("email in use")
  } else if (username && (await asServer.user.existsByUsername(username))) {
    throw new DuplicateError("username in use")
  }

  // call registration functiona
  const result = await asServer.user.register(validation.value)

  // return created user and token
  res.status(201).pack(true, result.user, { token: result.token })
}

const login = async (req: Request, res: Response) => {
  // validate
  const { value, error } = object({
    email: string().email().max(1024),
    username: Name,
    password: string().max(1024).required(),
  })
    .xor("email", "username")
    .validate(req.body, { stripUnknown: true })

  if (error) throw new MalformedContentError(error.message)

  const result = await (value.email
    ? asServer.user.loginWithEmail(value)
    : asServer.user.loginWithUsername(value))

  res.pack(true, result.user, { token: result.token })
}

export default { register, login }

import logger from "winston"
import { asServer } from "../../db"
import { AuthError } from "../../errors"
import { Request, Response } from "../../types/express"

/**
 * Deletes User and all their Plates
 */
const one = async (req: Request, res: Response) => {
  const { user_id } = req.params

  logger.debug("USER DELETE", { user_id })

  if (req.user.id !== user_id && req.user.role !== "ADMIN") {
    logger.debug("USER DELETE UNAUTHORIZED", { user_id, client_id: req.user.id })
    throw new AuthError()
  }

  // delete user
  const { id: userID } = await asServer.user.deleteById(user_id)
  // delete their plates
  await asServer.plate.deleteById(
    ...(await asServer.plate.search({ userID })).map(p => p.id)
  )

  res.pack(true, null, { deletedAt: new Date().toISOString() })
}

export default { one }
import { asServer } from "../../db"
import { DuplicateError, MalformedContentError } from "../../errors"
import { Request, Response } from "../../types/express"
import { Plate } from "../../models"
import { conformPlate } from "./util"

const one = async (req: Request, res: Response) => {
  // validate data
  const { value, error } = Plate.validate(req.body, { stripUnknown: true })

  if (error) throw new MalformedContentError(error.message)

  // Create Plate as User
  try {
    const plate = await asServer.plate.create({ ...value, userID: req.user.id })
    res.pack(true, conformPlate(plate, req.user))
  } catch (e) {
    if (e.message == "instance not unique") {
      throw new DuplicateError(
        `A Plate with name ${value.name} already exists under user ${req.user.username || req.user.email}`
      )
    } else throw e
  }
}

export default { one }
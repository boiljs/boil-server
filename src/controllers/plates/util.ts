import { PlateType, UserType } from "../../models"

export const conformPlate = (plate: PlateType, user: UserType) => {
  delete plate.userID
  return {
    ...plate,
    user: {
      id: user.id,
      username: user.username
    },
  }
}

export const userOwnsPlate = (user: UserType | string, plate: PlateType) => {
  return plate.userID === (typeof user === "string" ? user : user.id)
}
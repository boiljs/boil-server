import { asServer } from "../../db"
import { Request, Response } from "types/express"
import { userOwnsPlate } from "./util"
import { AuthError } from "../../errors"

const one = async (req: Request, res: Response) => {
  const { plate_id } = req.params

  const plate = await asServer.plate.getById(plate_id)

  if (userOwnsPlate(req.user, plate) || req.user.role === "ADMIN") {
    await asServer.plate.deleteById()
    res.pack(true, null, { deletedAt: new Date().toISOString() })
  } else {
    throw new AuthError()
  }
}

export default { one }
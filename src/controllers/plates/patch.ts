import { asServer } from "../../db"
import { AuthError, DuplicateError, MalformedContentError, ResourceNotFoundError } from "../../errors"
import { Request, Response } from "../../types/express"
import { PlateUpdatePartial } from "../../models"
import { conformPlate, userOwnsPlate } from "./util"

/**
 * PATCH /plates/{plate_id}
 */
const one = async (req: Request, res: Response) => {
  // validate data
  const { value, error } = PlateUpdatePartial.validate(req.body, { stripUnknown: true })

  if (error) throw new MalformedContentError(error.message)

  // Update
  let { plate_id } = req.params

  const plate = await asServer.plate.getById(plate_id)

  if (!(userOwnsPlate(req.user, plate) || plate.public || req.user.role === "ADMIN")) {
    throw new AuthError()
  }

  try {
    const updatedPlate = await asServer.plate.updateById(plate_id, value)
    res.pack(true, conformPlate(updatedPlate, req.user))
  } catch (e) {
    if (e.message == "instance not unique") {
      throw new DuplicateError(
        `A Plate with name ${value.name} already exists under user ${req.user.email}`
      )
    } else throw e
  }
}

export default { one }
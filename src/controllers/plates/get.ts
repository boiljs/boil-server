import logger from "winston"
import { asServer } from "../../db"
import { AuthError } from "../../errors"
import { Request, Response } from "../../types/express"
import { conformPlate, userOwnsPlate } from "./util"

/**
 * in:
 *  - plate_id [ID or platename]
 * out:
 *  - public API plate
 * 
 * GET /plates/{plate_id}
 */
const one = async (req: Request, res: Response) => {
  const { plate_id } = req.params

  // Get plate and owner
  const plate = await asServer.plate.getById(plate_id)

  // only show private plates to owner
  // TODO allow admins to get private plates (?)
  if (
    req.user?.role !== "ADMIN"
    && plate.public !== true
    && userOwnsPlate(req.user, plate)
  ) {
    throw new AuthError()
  }

  const user = await asServer.user.getByID(plate.userID)

  res.pack(true, conformPlate(plate, user))
}

type SearchQueries = {
  username?: string
  platename?: string
  /** ^(.+,){1,}$ */
  tags?: string
  page?: number
  size?: number
}

/**
 * in:
 *  - username?
 *  - platename?
 *  - tags? /^(.+,){1,}$/
 * out:
 *  - array of public API plates
 * GET /plates?...
 * 
 * TODO Plate Search Pagination
 */
const many = async (req: Request, res: Response) => {
  let { username, platename, tags }: SearchQueries = req.query
  let splitTags: string[]

  // transform tags
  if (tags) {
    splitTags = tags.split(',')
  }

  let userID: string

  if (username && await asServer.user.existsByUsername(username)) {
    userID = (await asServer.user.getByUsername(username)).id
  }

  logger.debug("PLATE GET MANY", { search: { userID, platename, tags: splitTags } })

  const searchResult = await asServer.plate.search({ userID, platename, tags: splitTags })

  // Hide unowned private plates
  let filteredResults: any[] = searchResult
    .filter(plate => (
      req.user?.role === "ADMIN"
      || plate.public
      || userOwnsPlate(req.user, plate)
    ))
    .map(async plate => {
      return conformPlate(
        plate,
        await asServer.user.getByID(plate.userID)
      )
    })

  filteredResults = await Promise.all(filteredResults)

  logger.debug("PLATE GET MANY", { searchResult, filteredResults })

  res.pack(true, filteredResults)
}

export default { one, many }
require("dotenv").config();
const { inStringChar } = require("randoms-js");
const { schema } = require("../../src/models/Name");

describe("JOI Schema: Name", () => {
  it("should allow valid", () => {
    for (let i = process.env.MODEL_NAME_MIN; i <= process.env.MODEL_NAME_MAX; i++) {
      let build = "";
      for (let k = 0; k < i; k++) {
        build += inStringChar(
          "ABCDEFGHIJKLMNOPQRSTUVWXYZ" +
            "ABCDEFGHIJKLMNOPQRSTUVWXYZ".toLowerCase() +
            inStringChar("-_")
        );
      }
      const result = schema.validate("a" + build);
      expect(result.value).toBeDefined();
    }
  });

  it("should not allow missing", () => {
    const result = schema.validate();
    expect(result.error).toBeDefined();
  });

  it("should not allow empty", () => {
    const result = schema.validate("");
    expect(result.error).toBeDefined();
  });

  it(`should not allow shorter than ${process.env.MODEL_NAME_MIN}`, () => {
    let result = schema.validate("a");
    expect(result.error).toBeDefined();

    result = schema.validate("ab");
    expect(result.error).toBeDefined();
  });

  it(`should not allow greater than ${process.env.MODEL_NAME_MAX}`, () => {
    for (let i = process.env.MODEL_NAME_MAX; i < process.env.TEST_LOOP; i++) {
      const result = schema.validate("a".repeat(i));
      expect(result.error).toBeDefined();
    }
  });

  it("should not allow starting with dash/hyphen (-)", () => {
    const result = schema.validate("-aa");
    expect(result.error).toBeDefined();
  });

  it("should not allow starting with underline (_)", () => {
    const result = schema.validate("_aa");
    expect(result.error).toBeDefined();
  });
});

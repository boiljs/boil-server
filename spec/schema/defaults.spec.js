require("dotenv").config();
const { schema } = require("../../src/models/Defaults");
const { ValidationError } = require("@hapi/joi");

describe("JOI Schema: Defaults", () => {
  describe("username", () => {
    it("should not allow missing", async () => {
      try {
        await schema.validateAsync();
      } catch (error) {
        expect(error).toBeInstanceOf(ValidationError);
        expect(error.message).toMatch("username");
      }
    });
  });

  describe("author", () => {
    it(`should allow valid from ${process.env.MODEL_NAME_MIN} to ${process.env.MODEL_NAME_MAX}`, async () => {
      for (let i = process.env.MODEL_NAME_MIN; i <= process.env.MODEL_NAME_MAX; i++) {
        await schema.validateAsync({ author: "a".repeat(i), username: "username" });
      }
    });

    it(`should not allow string less than ${process.env.MODEL_NAME_MIN}`, async () => {
      for (let i = 0; i < process.env.MODEL_NAME_MIN; i++) {
        try {
          await schema.validateAsync({ author: "a".repeat(i), username: "username" });
        } catch (error) {
          expect(error).toBeInstanceOf(ValidationError);
          expect(error.message).toMatch("author");
        }
      }
    });

    it(`should not allow string greater than ${process.env.MODEL_NAME_MAX}`, async () => {
      for (let i = process.env.MODEL_NAME_MAX; i < process.env.TEST_LOOP; i++) {
        try {
          await schema.validateAsync({ author: "a".repeat(i), username: "username" });
        } catch (error) {
          expect(error).toBeInstanceOf(ValidationError);
          expect(error.message).toMatch("author");
        }
      }
    });
  });

  // TODO license
  // TODO version
  // TODO homepage
  // TODO main

  describe("boil_meta", () => {
    describe("useSrc", () => {
      it("should allow true", async () => {
        await schema.validateAsync({ boil_meta: { useSrc: true }, username: "username" });
      });

      it("should allow false", async () => {
        await schema.validateAsync({
          boil_meta: { useSrc: false },
          username: "username"
        });
      });

      it("should not allow string", async () => {
        try {
          await schema.validateAsync({ boil_meta: { useSrc: "" }, username: "username" });
        } catch (error) {
          expect(error).toBeInstanceOf(ValidationError);
          expect(error.message).toMatch("useSrc");
        }
      });

      it("should not allow number", async () => {
        for (let i = process.env.TEST_LOOP * -1; i < process.env.TEST_LOOP; i++) {
          try {
            await schema.validateAsync({
              boil_meta: { useSrc: i },
              username: "username"
            });
          } catch (error) {
            expect(error).toBeInstanceOf(ValidationError);
            expect(error.message).toMatch("useSrc");
          }
        }
      });

      it("should not allow function", async () => {
        try {
          await schema.validateAsync({
            boil_meta: { useSrc: () => {} },
            username: "username"
          });
        } catch (error) {
          expect(error).toBeInstanceOf(ValidationError);
          expect(error.message).toMatch("useSrc");
        }
      });

      it("should not allow object", async () => {
        try {
          await schema.validateAsync({ boil_meta: { useSrc: {} }, username: "username" });
        } catch (error) {
          expect(error).toBeInstanceOf(ValidationError);
          expect(error.message).toMatch("useSrc");
        }
      });
    });
  });

  // TODO createdAt
  // TODO updatedAt
});

// TODO! Complete Missing Unit Tests:
//  user.email

require("dotenv").config();
const { schema } = require("../../src/models/User");
const { ValidationError } = require("@hapi/joi");

const fakeHash = "$2a$15$aaaaaaaaaaaaaaaaaaaaa";

describe("JOI Schema: User", () => {
  it("should allow valid", async () => {
    const value = await schema.validateAsync({
      username: "u_s-e_r-NAme_",
      passwordHash: fakeHash
    });

    expect(value.createdAt).not.toBeNaN();
    expect(value.updatedAt).not.toBeNaN();
  });

  describe("Password", () => {
    it("should not allow missing", async () => {
      try {
        await schema.validateAsync({ username: "username" });
      } catch (e) {
        expect(e).toBeInstanceOf(ValidationError);
        expect(e.message).toMatch("passwordHash");
      }
    });

    it("should not allow empty", async () => {
      try {
        await schema.validateAsync({ username: "username", passwordHash: "" });
      } catch (error) {
        expect(error).toBeInstanceOf(ValidationError);
        expect(error.message).toMatch("passwordHash");
      }
    });

    it("should not allow invalid", async () => {
      try {
        await schema.validateAsync({
          username: "username",
          passwordHash: "a"
        });
      } catch (e) {
        expect(e).toBeInstanceOf(ValidationError);
        expect(e.message).toMatch("passwordHash");
      }
    });
  });
});

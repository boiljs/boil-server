require("dotenv").config();
const { schema } = require("../../src/models/Version");
const { ValidationError } = require("@hapi/joi");

describe("JOI Schema: Version", () => {
  describe("Major Version", () => {
    it("should allow valid", async () => {
      for (let i = 0; i < 1000; i++) {
        await schema.validateAsync(`${i}.0.0`);
      }
    });

    it("should not allow missing", async () => {
      try {
        await schema.validate(".0.0");
      } catch (error) {
        expect(error).toBeInstanceOf(ValidationError);
      }
    });

    it("should not allow 4 digits", async () => {
      try {
        await schema.validateAsync("0000.0.0");
      } catch (error) {
        expect(error).toBeInstanceOf(ValidationError);
      }
    });

    it("should not allow non-digit", async () => {
      try {
        await schema.validateAsync("a.0.0");
      } catch (error) {
        expect(error).toBeInstanceOf(ValidationError);
      }
    });
  });

  describe("Minor Version", () => {
    it("should allow valid", async () => {
      for (let i = 0; i < 1000; i++) {
        await schema.validateAsync(`0.${i}.0`);
      }
    });

    it("should not allow missing", async () => {
      try {
        await schema.validateAsync("0..0");
      } catch (error) {
        expect(error).toBeInstanceOf(ValidationError);
      }
    });

    it("should not allow 4 digits", async () => {
      try {
        await schema.validateAsync("0.0000.0");
      } catch (error) {
        expect(error).toBeInstanceOf(ValidationError);
      }
    });

    it("should not allow non-digit", async () => {
      try {
        await schema.validateAsync("0.a.0");
      } catch (error) {
        expect(error).toBeInstanceOf(ValidationError);
      }
    });
  });

  describe("Patch Version", () => {
    it("should allow valid", async () => {
      for (let i = 0; i < 1000; i++) {
        await schema.validateAsync(`0.0.${i}`);
      }
    });

    it("should not allow missing patch version", async () => {
      try {
        await schema.validateAsync("0.0.");
      } catch (error) {
        expect(error).toBeInstanceOf(ValidationError);
      }
    });

    it("should not allow 4 digits", async () => {
      try {
        await schema.validateAsync("0.0.0000");
      } catch (error) {
        expect(error).toBeInstanceOf(ValidationError);
      }
    });

    it("should not allow non-digit", async () => {
      try {
        await schema.validateAsync("0.0.a");
      } catch (error) {
        expect(error).toBeInstanceOf(ValidationError);
      }
    });
  });
});

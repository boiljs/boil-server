const { schema } = require("../../src/models/Dependencies");
const { ValidationError } = require("@hapi/joi");

describe("JOI Schema: Dependencies", () => {
  it("should not non-object object", async () => {
    try {
      await schema.validateAsync("");
    } catch (error) {
      expect(error).toBeInstanceOf(ValidationError);
    }
  });

  it("should allow valid dependencies", async () => {
    try {
      await schema.validateAsync({ name: "0.0.0" });
    } catch (error) {
      expect(error).toBeInstanceOf(ValidationError);
    }
  });

  it("should not allow null values", async () => {
    try {
      await schema.validateAsync({ name: null });
    } catch (error) {
      expect(error).toBeInstanceOf(ValidationError);
    }
  });

  it("should not allow undefined values", async () => {
    try {
      await schema.validateAsync({ name: undefined });
    } catch (error) {
      expect(error).toBeInstanceOf(ValidationError);
    }
  });

  it("should not allow object values", async () => {
    try {
      await schema.validateAsync({ name: {} });
    } catch (error) {
      expect(error).toBeInstanceOf(ValidationError);
    }
  });

  it("should not allow boolean false", async () => {
    try {
      await schema.validateAsync({ name: false });
    } catch (error) {
      expect(error).toBeInstanceOf(ValidationError);
    }
  });

  it("should not allow boolean true", async () => {
    try {
      await schema.validateAsync({ name: true });
    } catch (error) {
      expect(error).toBeInstanceOf(ValidationError);
    }
  });

  it("should not allow function values", async () => {
    try {
      await schema.validateAsync({ name: () => {} });
    } catch (error) {
      expect(error).toBeInstanceOf(ValidationError);
    }
  });

  it("should not allow number values", async () => {
    for (let i = 0; i < process.env.TEST_LOOP; i++) {
      try {
        await schema.validateAsync({ name: i });
      } catch (error) {
        expect(error).toBeInstanceOf(ValidationError);
      }
    }
  });
});

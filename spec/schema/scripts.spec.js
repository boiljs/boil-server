const { schema } = require("../../src/models/Scripts");

describe("JOI Schema: Scripts", () => {
  it("should allow valid scripts", () => {
    const result = schema.validate({ name: "action" });
    expect(result.value).toBeDefined();
  });

  it("should not allow null values", () => {
    const result = schema.validate({ name: null });
    expect(result.error).toBeDefined();
  });

  it("should not allow undefined values", () => {
    const result = schema.validate({ name: undefined });
    expect(result.error).toBeDefined();
  });

  it("should not allow object values", () => {
    const result = schema.validate({ name: {} });
    expect(result.error).toBeDefined();
  });

  it("should not allow boolean values", () => {
    let result = schema.validate({ name: false });
    expect(result.error).toBeDefined();
    result = schema.validate({ name: true });
    expect(result.error).toBeDefined();
  });

  it("should not allow function values", () => {
    const result = schema.validate({ name: () => {} });
    expect(result.error).toBeDefined();
  });

  it("should not allow number values", () => {
    for (let i = 0; i < process.env.TEST_LOOP; i++) {
      const result = schema.validate({ name: i });
      expect(result.error).toBeDefined();
    }
  });
});
